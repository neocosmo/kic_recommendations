<?php

namespace Drupal\kic_recommendations\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Module configuration form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'kic_recommendations.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'kic_recommendations_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config(static::SETTINGS);
    $form['endpoint_global'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Global context API endpoint'),
      '#default_value' => $config->get('endpoint_global'),
      '#description' => $this->t('API endpoint URL of the global context. The URL may contain "%uuid" which gets replaced by the Drupal-UUID of the node for which the request is made.'),
      '#required' => TRUE,
    ];
    $form['access_token'] = [
      '#type' => 'container',
      'label' => [
        '#type' => 'html_tag',
        '#tag' => 'label',
        '#value' => $this->t('API Access Token'),
      ],
      'description' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['description'],
        ],
        'text' => [
          '#type' => 'markup',
          '#markup' => $this->t('Set the access token via your <em>settings.php</em>. Use <code>$config["kic_learn.settings"]["access_token"] = "..."</code>.<br>Setting the access token via the <em>settings.php</em> file, prevents the token from being commited to a VCS via a configuration export.'),
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $config->set('endpoint_global', $form_state->getValue('endpoint_global'));
    $config->set('access_token', $form_state->getValue('access_token'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

}
