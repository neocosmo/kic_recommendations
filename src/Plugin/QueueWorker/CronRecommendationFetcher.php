<?php

namespace Drupal\kic_recommendations\Plugin\QueueWorker;

use Drupal\kic_recommendations\Form\SettingsForm;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A recommendation fetcher that runs on cron runs.
 *
 * @QueueWorker(
 *   id = "cron_recommendation_fetcher",
 *   title = @Translation("Cron Recommendation Fetcher"),
 *   cron = {"time" = 10}
 * )
 */
class CronRecommendationFetcher extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * A logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $dbconn;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Module configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')->get('kic_recommendations'),
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('datetime.time'),
      $container->get('config.factory')->get(SettingsForm::SETTINGS)
    );
  }

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   A logger.
   * @param \Drupal\Core\Database\Connection $dbconn
   *   A database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   An entity type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   A time object.
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   The module configuration.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    LoggerChannelInterface $logger,
    Connection $dbconn,
    EntityTypeManagerInterface $entity_type_manager,
    TimeInterface $time,
    ImmutableConfig $config) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger;
    $this->dbconn = $dbconn;
    $this->entityTypeManager = $entity_type_manager;
    $this->time = $time;
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $ref_nid = $data->nid;
    $uuid = $data->uuid;
    $context_type = $data->context_type;

    $recommendations = $this->fetchRecommendations($uuid);
    if ($recommendations === FALSE) {
      return;
    }

    // The transaction is committed when the variable is being unset/goes out of
    // scope.
    $transaction = $this->dbconn->startTransaction();

    try {
      $request_time = $this->time->getRequestTime();
      $this->dbconn->delete('kic_recommendations_updated')
        ->condition('ref_nid', $ref_nid, '=')
        ->condition('context_type', $context_type, '=')
        ->execute();
      $this->dbconn->insert('kic_recommendations_updated')
        ->fields([
          'ref_nid' => $ref_nid,
          'updated' => $request_time,
          'context_type' => $context_type,
        ])
        ->execute();

      $this->dbconn->delete('kic_recommendations_global')
        ->condition('ref_nid', $ref_nid, '=')
        ->execute();
      $query = $this->dbconn
        ->insert('kic_recommendations_global')
        ->fields(['ref_nid', 'rec_nid', 'score', 'type']);

      // Keep track of the recommendations already inserted as the server may
      // send duplicated data.
      $inserted_rec_nids = [];
      foreach ($recommendations as $rec) {
        $rec_uuid = $rec->recommended_learning_offer_id_neo;
        $rec_nid = $this->getNidByUuid($rec_uuid);
        if ($rec_nid === 0) {
          continue;
        }
        if (isset($inserted_rec_nids[$rec_nid])) {
          $this->logger->warning('Duplicate recommendation "@r" for node "@n".', [
            '@r' => $rec_nid,
            '@n' => $ref_nid,
          ]);
          continue;
        }
        $query->values([
          'ref_nid' => $ref_nid,
          'rec_nid' => $rec_nid,
          'score' => (double) $rec->strength_of_recommendation,
          'type' => $rec->type_of_recommendation,
        ]);
        $inserted_rec_nids[$rec_nid] = TRUE;
      }
      $query->execute();
    }
    catch (Exception $e) {
      $transaction->rollBack();
      $this->logger->warning('Failed to update recommendations for "@n": @e', [
        '@n' => $uuid,
        '@e' => $e->getMessage(),
      ]);
    }
  }

  /**
   * Get a node's ID via its UUID.
   *
   * @param string $uuid
   *   A node's UUID.
   *
   * @return int
   *   The node's ID or 0, if no node could be found with the provided UUID.
   */
  protected function getNidByUuid(string $uuid) {
    $id_map = &drupal_static(__METHOD__, []);
    if (isset($id_map[$uuid])) {
      return $id_map[$uuid];
    }

    $node_storage = $this->entityTypeManager->getStorage('node');
    $nodes_by_uuid = $node_storage->loadByProperties(['uuid' => $uuid]);
    if (count($nodes_by_uuid) !== 1) {
      $this->logger->warning('Failed to load node by UUID "@u".', ['@u' => $uuid]);
      // Remove all results from the array. This ensures that we do not return
      // an ID, if for a UUID there are more than one nodes found.
      $nodes_by_uuid = [];
    }

    $id_map[$uuid] = array_keys($nodes_by_uuid)[0] ?? 0;
    return $id_map[$uuid];
  }

  /**
   * Fetch recommendations from the recommendation server.
   *
   * @param string $uuid
   *   The UUID of the node for which to fetch the recommendations.
   *
   * @return mixed
   *   An array of stdClass objects. Each object has the following properties:
   *   - for_learning_offer: KIC UUID of the learning offer for which the
   *     recommendation is requested.
   *   - for_learning_offer_neo: Drupal UUID of the learning offer for which the
   *     recommendation is requested.
   *   - recommended_learning_offer: DFKI UUID of the recommended learning
   *     offer.
   *   - recommended_learning_offer_id_neo: DFKI Drupal UUID of the recommended
   *     learning offer.
   *   - type_of_recommendation: Type of recommendation. Is either similar ('s')
   *     or continuative ('c').
   *   - strength_of_recommendation: Strength of the calculated recommendation.
   *     The value is in the interval [-1, 1] and increases with increasing
   *     number value.
   *   If fetching the recommendations fails, FALSE is returned.
   */
  protected function fetchRecommendations(string $uuid) {
    $token = $this->config->get('access_token');
    $url = $this->config->get('endpoint_global');

    $url = str_replace('%uuid', $uuid, $url);
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
      'Authorization: Token ' . $token,
      'Accept: application/json',
    ]);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $res = curl_exec($ch);
    if ($res === FALSE) {
      $this->logger->warning('Failed to fetch recommendations for UUID @u: @e', [
        '@u' => $uuid,
        '@e' => curl_error($ch),
      ]);
      return FALSE;
    }
    $recommendations = json_decode($res);
    if (!is_array($recommendations)) {
      $this->logger->warning('Recommendations API endpoint returned unexpected response:<br>@r', [
        '@r' => $res,
      ]);
      return [];
    }
    return $recommendations;
  }

}
