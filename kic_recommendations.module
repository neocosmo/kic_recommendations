<?php

/**
 * @file
 * KI-Campus recommendations module file.
 */

use Drupal\Core\Database\Query\Condition;

define('KIC_RECOMMENDATIONS_CONTEXT_TYPE_GLOBAL', 'global');

/**
 * Implements hook_views_data_alter().
 */
function kic_recommendations_views_data() {
  return [
    'kic_recommendations_global' => [
      'table' => [
        'group' => t('KIC Recommendations'),
        'provider' => 'kic_recommendations',
      ],
      'ref_nid' => [
        'title' => t('Reference node'),
        'help' => t('Reference node of recommendations.'),
        'relationship' => [
          'base' => 'node_field_data',
          'base field' => 'nid',
          'id' => 'standard',
          'label' => t('Reference node'),
        ],
        'filter' => [
          'id' => 'standard',
        ],
        'argument' => [
          'id' => 'numeric',
        ],
      ],
      'rec_nid' => [
        'title' => t('Recommended node'),
        'help' => t('Node recommended in reference to another node.'),
        'relationship' => [
          'base' => 'node_field_data',
          'base field' => 'nid',
          'id' => 'standard',
          'label' => t('Reference node'),
        ],
      ],
      'score' => [
        'title' => t('Recommendation score'),
        'help' => t('Score of recommendation'),
        'sort' => [
          'id' => 'standard',
        ],
      ],
      'type' => [
        'title' => t('Recommendation type'),
        'help' => t('Type of recommendation'),
        'filter' => [
          'id' => 'string',
        ],
      ],
    ],
  ];
}

/**
 * Implements hook_views_data_alter().
 */
function kic_recommendations_views_data_alter(&$data) {
  $data['node']['kic_recommendations_global_reference'] = [
    'title' => t('Recommendations reference node'),
    'help' => t('Node with global recommendations.'),
    'relationship' => [
      'base' => 'kic_recommendations_global',
      'base field' => 'ref_nid',
      'field' => 'nid',
      'id' => 'standard',
      'label' => t('Recommendation reference node'),
    ],
  ];
  $data['node']['kic_recommendations_global_recommendation'] = [
    'title' => t('Recommended node'),
    'help' => t('Node recommended via other nodes.'),
    'relationship' => [
      'base' => 'kic_recommendations_global',
      'base field' => 'rec_nid',
      'field' => 'nid',
      'id' => 'standard',
      'label' => t('Recommended node'),
    ],
  ];
}

/**
 * Implements hook_cron().
 */
function kic_recommendations_cron() {
  $types = kic_recommendations_content_types();
  if (empty($types)) {
    return;
  }

  $context_type = KIC_RECOMMENDATIONS_CONTEXT_TYPE_GLOBAL;
  $outdated_global = kic_recommendations_outdated($context_type);

  $queue_factory = \Drupal::service('queue');
  $queue = $queue_factory->get('cron_recommendation_fetcher');
  foreach ($outdated_global as $ref_node) {
    $item = new \stdClass();
    $item->nid = $ref_node->nid;
    $item->uuid = $ref_node->uuid;
    $item->context_type = $context_type;
    $queue->createItem($item);
  }
}

/**
 * Get an array of nodes with outdated or not-existent recommendations.
 *
 * @param string $context_type
 *   The context type of the recommendations.
 *
 * @return array
 *   An array of stdClass objects. Each object has the properties 'nid' and
 *   'uuid' of a node for which the recommendations are outdated or do not
 *   exist.
 */
function kic_recommendations_outdated(string $context_type) {
  $types = kic_recommendations_content_types();
  if (empty($types)) {
    return [];
  }

  $query = \Drupal::database()
    ->select('node', 'n')
    ->fields('n', ['nid', 'uuid'])
    ->condition('n.type', $types, 'IN');
  $krg_alias = $query->leftJoin('kic_recommendations_updated', 'krg',
    '%alias.ref_nid = n.nid');

  $cond_outdated = (new Condition('OR'))->isNull("$krg_alias.updated");
  $ttl = kic_recommendations_ttl();
  if ($ttl > 0) {
    $request_time = \Drupal::time()->getRequestTime();
    $time_outdated = $request_time - $ttl;
    $cond_outdated->condition("$krg_alias.updated", $time_outdated, '<=');
  }

  $cond_context_type = (new Condition('OR'))
    ->condition("$krg_alias.context_type", $context_type, '=')
    ->isNull("$krg_alias.context_type");

  $res = $query
    ->condition($cond_context_type)
    ->condition($cond_outdated)
    ->distinct()
    ->execute();
  return $res->fetchAll();
}

/**
 * Fetch recommendations for a node and store them in the database.
 *
 * @param string $uuid
 *   The UUID of the node for which to fetch the recommendations.
 */
function kic_recommendations_fetch(string $uuid) {
}

/**
 * Get content types for which recommendations are enabled.
 *
 * @return array
 *   An array of content type machine names.
 *
 * @todo Implement a configuration form for this.
 */
function kic_recommendations_content_types() {
  return ['course'];
}

/**
 * Get the time-to-live for recommendations.
 *
 * @return int
 *   The duration in seconds for which recommendations are valid. If 0, the
 *   recommendations do not expire.
 *
 * @todo Implement a configuration form for this.
 */
function kic_recommendations_ttl() {
  return 60 * 60 * 24;
}
